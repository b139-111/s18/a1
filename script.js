//
let trainer = {
	Name: "Ash Ketchum",
	Age: 10,
	Pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	Friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log(`${trainer.Pokemon[0]}! I choose you!`);
	}
}

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.Name);
console.log("Result of square bracket notation:");
console.log(trainer["Pokemon"]);
console.log("Result of talk method:")
trainer.talk();

// 
function Pokemon(Name, Level){
	this.name = Name;
	this.level = Level;
	this.health = Level * 2;
	this.attack = Level;

	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`);
		console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`)
		if (target.health - this.attack <=0) {
			console.log(`${target.name} fainted`)
			}
		let newTarget ={
			Name : target.name,
			Level : target.level,
			Health : target.health - this.attack,
			Attack : target.attack,
			Tackle : target.tackle
		}
		console.log(newTarget)	
	}
}
	
let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);
geodude.tackle(pikachu);
mewtwo.tackle(geodude);